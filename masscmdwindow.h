#ifndef MASSCMDWINDOW_H
#define MASSCMDWINDOW_H

#include <QWidget>
#include <QStatusBar>
#include <QProgressBar>
#include "server.h"
#include "sshmanager.h"

namespace Ui {
class MassCmdWindow;
}

class MassCmdWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MassCmdWindow(QString *login, QString *passwd, QStatusBar *statusBar, QVector<Server*> *serverList, QWidget *parent = 0);
    ~MassCmdWindow();

private slots:
    void on_massCmdSend_returnPressed();

private:
    Ui::MassCmdWindow *ui;
    QString *login;
    QString *passwd;
    QStatusBar *statusBar;
    QVector<Server*> *serverList;
    QWidget *parent;
};

#endif // MASSCMDWINDOW_H
