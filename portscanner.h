#ifndef PORTSCANNER_H
#define PORTSCANNER_H

#include <QObject>
#include <QTcpSocket>

class PortScanner : public QObject
{
    Q_OBJECT
public:
    explicit PortScanner(int index, QObject *parent = 0);
    void scan(QString ip, quint16 port);
    void end();

signals:
    void resultRecieved(int index, QString ip, quint16 port, bool result);

public slots:
    void on_tcp_connected();
    void on_tcp_error(QAbstractSocket::SocketError);

private:
    QString ip;
    quint16 port;
    QTcpSocket socket;
    bool result;
    int index;

};

#endif // PORTSCANNER_H
