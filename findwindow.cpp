#include "findwindow.h"
#include "ui_findwindow.h"

FindWindow::FindWindow(QString *login, QString *passwd, QStatusBar *statusBar, QVector<Server*> *serverList, QWidget *parent) : QWidget(parent),
    ui(new Ui::FindWindow)
{
    ui->setupUi(this);
    this->login=login;
    this->passwd=passwd;
    this->statusBar=statusBar;
    this->serverList=serverList;
}

FindWindow::~FindWindow()
{
    delete ui;
}
