#ifndef COPYWINDOW_H
#define COPYWINDOW_H

#include <QWidget>
#include <QStatusBar>
#include "server.h"

namespace Ui {
class CopyWindow;
}

class CopyWindow : public QWidget
{
    Q_OBJECT

public:
    explicit CopyWindow(QString *login, QString *passwd, QStatusBar *statusBar, QVector<Server*> *serverList, QWidget *parent = 0);
    ~CopyWindow();

private:
    Ui::CopyWindow *ui;
    QString *login;
    QString *passwd;
    QStatusBar *statusBar;
    QVector<Server*> *serverList;
};

#endif // COPYWINDOW_H
