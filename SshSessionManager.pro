#-------------------------------------------------
#
# Project created by QtCreator 2014-08-23T19:17:01
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SshSessionManager
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    userdialog.cpp \
    settingswindow.cpp \
    connectwindow.cpp \
    copywindow.cpp \
    scanwindow.cpp \
    findwindow.cpp \
    masscmdwindow.cpp \
    server.cpp \
    sshmanager.cpp \
    portscanner.cpp

HEADERS  += mainwindow.h \
    userdialog.h \
    connectwindow.h \
    settingswindow.h \
    copywindow.h \
    scanwindow.h \
    findwindow.h \
    masscmdwindow.h \
    server.h \
    sshmanager.h \
    portscanner.h

FORMS    += userdialog.ui \
    connectwindow.ui \
    settingswindow.ui \
    copywindow.ui \
    scanwindow.ui \
    findwindow.ui \
    masscmdwindow.ui

RESOURCES += rc.qrc

copyStylesheet.commands = $(COPY_DIR) $$PWD/stylesheet.css $$OUT_PWD
copyImg.commands = $(COPY_DIR) $$PWD/img $$OUT_PWD
copyServerList.commands = $(COPY_DIR) $$PWD/serverList $$OUT_PWD
first.depends = $(first) copyStylesheet copyImg copyServerList
export(first.depends)
export(copyStylesheet.commands)
export(copyImg)
export(copyServerList)
QMAKE_EXTRA_TARGETS += first copyStylesheet copyImg copyServerList

unix {
   LIBS += -lssh
}

win32 {
   LIBS += -Llib/libssh_static.a -lssh
   INCLUDEPATH += include
}
