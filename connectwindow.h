#ifndef CONNECTWINDOW_H
#define CONNECTWINDOW_H

#include <QWidget>
#include <QStatusBar>
#include "server.h"

namespace Ui {
class ConnectWindow;
}

class ConnectWindow : public QWidget
{
    Q_OBJECT

public:
    explicit ConnectWindow(QString *login, QString *passwd, QStatusBar *satusBar, QVector<Server*> *serverList, QWidget *parent = 0);
    void updateServerComboList();
    ~ConnectWindow();

private slots:
    void on_connectButton_clicked();
    void on_connectSend_returnPressed();

private:
    Ui::ConnectWindow *ui;
    QString *login;
    QString *passwd;
    QStatusBar *statusBar;
    QVector<Server*> *serverList;
    QVector<int> *comboBoxToServerListIndex;
};

#endif // CONNECTWINDOW_H
