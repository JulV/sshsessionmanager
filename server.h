#ifndef SERVER_H
#define SERVER_H

#include <QWidget>
#include "portscanner.h"

class Server
{
    Q_ENUMS(State)
public:
    enum State{Reachable, Unreachable, Undefined};
    Server(QString &ip, State state);
    QString getIp();
    State getState();
    void setState(State state);
    PortScanner* getPortScanner();
    void setPortScanner(PortScanner *scanner);
    QStringList getUsersNames();
    void addUserName(QString username);
    void addUsersNames(QStringList usernameList);
    void clearUsersNames();
    ~Server();

private:
    QString *ip;
    State state;
    PortScanner *portScanner;
    QStringList *usersNames;
};

#endif // SERVER_H
