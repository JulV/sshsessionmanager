#ifndef SCANWINDOW_H
#define SCANWINDOW_H

#include <QWidget>
#include <QStatusBar>
#include <QProgressBar>
#include "server.h"
#include "portscanner.h"
#include "sshmanager.h"

namespace Ui {
class ScanWindow;
}

class ScanWindow : public QWidget
{
    Q_OBJECT
    Q_ENUMS(ScanMode)

public:
    enum ScanMode{StateOnly, ShowUsers};
    explicit ScanWindow(QString *login, QString *passwd, QStatusBar *statusBar, QVector<Server*> *serverList, QWidget *parent = 0);
    ~ScanWindow();

private slots:
    void on_scanButton_clicked();
    void scanResult(int index, QString ip, quint16 port, bool result);

private:
    void showServerList(ScanMode mode);
    Ui::ScanWindow *ui;
    QWidget *parent;
    QString *login;
    QString *passwd;
    QStatusBar *statusBar;
    QVector<Server*> *serverList;
    QProgressBar *progress;
};

#endif // SCANWINDOW_H
