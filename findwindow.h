#ifndef FINDWINDOW_H
#define FINDWINDOW_H

#include <QWidget>
#include <QStatusBar>
#include "server.h"

namespace Ui {
class FindWindow;
}

class FindWindow : public QWidget
{
    Q_OBJECT

public:
    explicit FindWindow(QString *login, QString *passwd, QStatusBar *statusBar, QVector<Server*> *serverList, QWidget *parent = 0);
    ~FindWindow();

private:
    Ui::FindWindow *ui;
    QString *login;
    QString *passwd;
    QStatusBar *statusBar;
    QVector<Server*> *serverList;
};

#endif // FINDWINDOW_H
