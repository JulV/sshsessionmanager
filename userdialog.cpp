#include "userdialog.h"
#include "ui_userdialog.h"

UserDialog::UserDialog(QString *login, QString *passwd, QWidget *parent) : QDialog(parent),
    ui(new Ui::UserDialog)
{
    ui->setupUi(this);
    this->setFixedSize(300, 150);
    this->login=login;
    this->passwd=passwd;
    ui->loginTextEdit->setText(*login);
    ui->passwdTextEdit->setText(*passwd);
    ui->loginTextEdit->setPlaceholderText(tr("Utilisateur"));
    ui->passwdTextEdit->setPlaceholderText(tr("Mot de passe"));
}

void UserDialog::on_submitButton_clicked()
{
    if (!ui->loginTextEdit->text().isEmpty() && !ui->passwdTextEdit->text().isEmpty())
    {
        *login=ui->loginTextEdit->text();
        *passwd=ui->passwdTextEdit->text();
        close();
    }
}

void UserDialog::on_loginTextEdit_returnPressed()
{
    on_submitButton_clicked();
}

void UserDialog::on_passwdTextEdit_returnPressed()
{
    on_submitButton_clicked();
}

UserDialog::~UserDialog()
{
    delete ui;
}
