#include "masscmdwindow.h"
#include "ui_masscmdwindow.h"

MassCmdWindow::MassCmdWindow(QString *login, QString *passwd, QStatusBar *statusBar, QVector<Server*> *serverList, QWidget *parent) : QWidget(parent),
    ui(new Ui::MassCmdWindow)
{
    ui->setupUi(this);
    ui->massCmdSend->setPlaceholderText(tr("Envoyer une commande en masse..."));
    this->login=login;
    this->passwd=passwd;
    this->statusBar=statusBar;
    this->serverList=serverList;
    this->parent=parent;
}

MassCmdWindow::~MassCmdWindow()
{
    delete ui;
}

void MassCmdWindow::on_massCmdSend_returnPressed()
{
    QString cmd = ui->massCmdSend->text();
    parent->setEnabled(false);
    ui->massCmdSend->clear();
    QProgressBar *progress = new QProgressBar();
    progress->setRange(1, serverList->size());
    statusBar->addPermanentWidget(progress);
    for (int i=0; i<serverList->size(); i++)
    {
        progress->setValue(i);
        if (serverList->at(i)->getState() == Server::Reachable)
        {
            ui->massCmdView->append("********************<br />"+serverList->at(i)->getIp()+tr(" a répondu :")+"<br />********************");
            ui->massCmdView->append(SshManager::execRemoteCmd(serverList->at(i)->getIp(), *login, *passwd, cmd));
        }
    }
    statusBar->removeWidget(progress);
    delete progress;
    parent->setEnabled(true);
    statusBar->showMessage(cmd+tr(" effectué"), 2500);
}
