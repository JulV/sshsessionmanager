#include "server.h"

Server::Server(QString &ip, State state)
{
    this->ip = new QString(ip);
    usersNames = new QStringList();
    this->state=state;
}

QString Server::getIp()
{
    return *ip;
}

Server::State Server::getState()
{
    return state;
}

void Server::setState(State state)
{
    this->state=state;
}

PortScanner* Server::getPortScanner()
{
    return portScanner;
}

void Server::setPortScanner(PortScanner* scanner)
{
    portScanner=scanner;
}

QStringList Server::getUsersNames()
{
    return *usersNames;
}

void Server::addUserName(QString username)
{
    usersNames->push_back(username);
}

void Server::addUsersNames(QStringList usernameList)
{
    *usersNames+=usernameList;
}

void Server::clearUsersNames()
{
    usersNames->clear();
}

Server::~Server()
{
    delete ip;
    delete usersNames;
}
