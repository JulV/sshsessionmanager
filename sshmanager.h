#ifndef SSHMANAGER_H
#define SSHMANAGER_H

#include <QString>
#include <libssh/libssh.h>

class SshManager
{
public:
    SshManager();
    static QString execRemoteCmd(QString ip, QString login, QString password, QString cmd);

private:
    static int openSession(ssh_session *session, const char *ip, const char *login, const char *password);
    static void closeSession(ssh_session *session);
    static int verifyKnownHost(ssh_session session);
    static QString requestCmdExec(ssh_session session, QString cmd);
};

#endif // SSHMANAGER_H
