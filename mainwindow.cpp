#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QWidget(parent)
{
    setWindowTitle("SSH Session Manager");
    login = new QString();
    passwd = new QString();
    statusBar = new QStatusBar();
    serverList = new QVector<Server*>();
    UserDialog *uDialog = new UserDialog(login, passwd, this);
    uDialog->exec();
    loadServerList(new QFile("serverList/serverList.txt"));
    connectWindow = new ConnectWindow(login, passwd, statusBar, serverList, this);
    tabs = new QTabWidget();
    tabs->addTab(new ScanWindow(login, passwd, statusBar, serverList, this), tr("Scanner"));
    tabs->addTab(connectWindow, tr("Connexion"));
    tabs->addTab(new CopyWindow(login, passwd, statusBar, serverList, this), tr("Copier"));
    tabs->addTab(new FindWindow(login, passwd, statusBar, serverList, this), tr("Rechercher"));
    tabs->addTab(new MassCmdWindow(login, passwd, statusBar, serverList, this), tr("Commande en masse"));
    tabs->addTab(new SettingsWindow(login, passwd, statusBar, this), tr("Paramètres"));
    setMinimumSize(600, 400);
    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addWidget(tabs);
    mainLayout->addWidget(statusBar);
    setLayout(mainLayout);
    connect(tabs, SIGNAL(currentChanged(int)), this, SLOT(processUpdates(int)));
    statusBar->showMessage(tr("Initialisation terminée"), 1000);
}

void MainWindow::loadServerList(QFile *file)
{
    if (file->open(QIODevice::ReadOnly))
    {
        QTextStream in(file);
        QString line;
        while (!in.atEnd())
        {
            line = in.readLine();
            serverList->push_back(new Server(line, Server::Undefined));
        }
        file->close();
    }
    else
    {
        statusBar->showMessage(tr("impossible de charger la liste des serveurs"), 5000);
        qDebug("Impossible de charger la liste des serveurs");
    }
}

void MainWindow::processUpdates(int tabIndex)
{
    switch (tabIndex)
    {
    case 1:
        connectWindow->updateServerComboList();
        break;
    }
}

MainWindow::~MainWindow()
{
    delete login;
    delete passwd;
    delete statusBar;
    delete serverList;
    delete tabs;
}
