#include "settingswindow.h"
#include "ui_settingswindow.h"

SettingsWindow::SettingsWindow(QString *login, QString *passwd, QStatusBar *statusBar, QWidget *parent) : QWidget(parent),
    ui(new Ui::SettingsWindow)
{
    ui->setupUi(this);
    this->login=login;
    this->passwd=passwd;
    this->statusBar=statusBar;
    ui->userLabel->setText(tr("Utilisateur courant : ")+*login);
}

void SettingsWindow::on_switchUserButton_clicked()
{
    UserDialog *uDialog = new UserDialog(login, passwd, this);
    uDialog->exec();
    ui->userLabel->setText(tr("Utilisateur courant : ")+*login);
    statusBar->showMessage(*login+tr(" est le nouvel utilisateur"), 3500);
}

SettingsWindow::~SettingsWindow()
{
    delete ui;
}
