#include "portscanner.h"

PortScanner::PortScanner(int index, QObject *parent) : QObject(parent)
{
    this->index=index;
}

void PortScanner::scan(QString ip, quint16 port)
{
    this->ip=ip;
    this->port=port;
    connect(&socket, SIGNAL(connected()), this, SLOT(on_tcp_connected()));
    connect(&socket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(on_tcp_error(QAbstractSocket::SocketError)));
    socket.connectToHost(ip, port);
}

void PortScanner::on_tcp_connected()
{
    result=true;
    end();
}

void PortScanner::on_tcp_error(QAbstractSocket::SocketError)
{
    result=false;
    end();
}

void PortScanner::end()
{
    emit resultRecieved(index, ip, port, result);
    this->deleteLater();
}
