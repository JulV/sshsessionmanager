#ifndef SETTINGSWINDOW_H
#define SETTINGSWINDOW_H

#include <QWidget>
#include <QStatusBar>
#include "userdialog.h"

namespace Ui {
class SettingsWindow;
}

class SettingsWindow : public QWidget
{
    Q_OBJECT

public:
    explicit SettingsWindow(QString *login, QString *passwd, QStatusBar *statusBar, QWidget *parent = 0);
    ~SettingsWindow();

private slots:
    void on_switchUserButton_clicked();

private:
    Ui::SettingsWindow *ui;
    QString *login;
    QString *passwd;
    QStatusBar *statusBar;
};

#endif // SETTINGSWINDOW_H
