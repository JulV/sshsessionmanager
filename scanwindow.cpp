#include "scanwindow.h"
#include "ui_scanwindow.h"

ScanWindow::ScanWindow(QString *login, QString *passwd, QStatusBar *statusBar, QVector<Server*> *serverList, QWidget *parent) : QWidget(parent),
    ui(new Ui::ScanWindow)
{
    ui->setupUi(this);
    this->parent=parent;
    this->login=login;
    this->passwd=passwd;
    this->statusBar=statusBar;
    this->serverList=serverList;
    showServerList(ScanWindow::StateOnly);
}

void ScanWindow::showServerList(ScanMode mode)
{
    ui->scanView->clear();
    QString stringBuffer;
    for (int i=0; i<serverList->size(); i++)
    {
        switch (serverList->at(i)->getState())
        {
        case Server::Unreachable:
            stringBuffer="img/statusOff.png";
            break;
        case Server::Reachable:
            stringBuffer="img/statusOn.png";
            break;
        default :
            stringBuffer="img/statusUnknown.png";
            break;
        }
        if (mode == ScanWindow::StateOnly || !(serverList->at(i)->getState() == Server::Reachable))
        {
            stringBuffer="<img src=\""+stringBuffer+"\" />"+serverList->at(i)->getIp()+"<br />";
        }
        else
        {
            stringBuffer="<img src=\""+stringBuffer+"\" />"+serverList->at(i)->getIp()+" : ";
            for (int j=0; j<serverList->at(i)->getUsersNames().size(); j++)
            {
                stringBuffer+=serverList->at(i)->getUsersNames().at(j)+", ";
            }
            stringBuffer=stringBuffer.left(stringBuffer.size()-2);
            stringBuffer+="<br />";
        }
        ui->scanView->insertHtml(stringBuffer);
    }
}

void ScanWindow::on_scanButton_clicked()
{
    parent->setEnabled(false);
    progress = new QProgressBar();
    progress->setRange(1, serverList->size());
    statusBar->addPermanentWidget(progress);
    for (int i=0; i<serverList->size(); i++)
    {
        serverList->at(i)->clearUsersNames();
        serverList->at(i)->setPortScanner(new PortScanner(i, this));
        connect(serverList->at(i)->getPortScanner(), SIGNAL(resultRecieved(int,QString,quint16,bool)), this, SLOT(scanResult(int,QString,quint16,bool)));
        serverList->at(i)->getPortScanner()->scan(serverList->at(i)->getIp(), 22);
    }
}

void ScanWindow::scanResult(int index, QString ip, quint16 port, bool result)
{
    if (result)
    {
        serverList->at(index)->setState(Server::Reachable);
        if (ui->scanUser->isChecked())
        {
            serverList->at(index)->addUsersNames(SshManager::execRemoteCmd(serverList->at(index)->getIp(), *login, *passwd, "users").split(" "));
        }
    }
    else
    {
        serverList->at(index)->setState(Server::Unreachable);
    }
    progress->setValue(progress->value()+1);
    if (progress->value() == serverList->size())
    {
        if (ui->scanUser->isChecked())
        {
            showServerList(ScanWindow::ShowUsers);
        }
        else
        {
            showServerList(ScanWindow::StateOnly);
        }
        parent->setEnabled(true);
        statusBar->removeWidget(progress);
        statusBar->showMessage(tr("Scan terminé"), 2500);
    }
}

ScanWindow::~ScanWindow()
{
    delete ui;
}
