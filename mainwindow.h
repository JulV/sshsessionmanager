#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QTabWidget>
#include <QStatusBar>
#include <QVBoxLayout>
#include <QFile>
#include <QTextStream>
#include "userdialog.h"
#include "connectwindow.h"
#include "settingswindow.h"
#include "copywindow.h"
#include "scanwindow.h"
#include "findwindow.h"
#include "masscmdwindow.h"
#include "server.h"

class MainWindow : public QWidget
{
    Q_OBJECT
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

signals:

private slots:
    void processUpdates(int tabIndex);

private:
    void loadServerList(QFile *file);
    QTabWidget *tabs;
    QStatusBar *statusBar;
    QString *login;
    QString *passwd;
    QVector<Server*> *serverList;
    ConnectWindow *connectWindow;

};

#endif // MAINWINDOW_H
