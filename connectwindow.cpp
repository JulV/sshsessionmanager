#include "connectwindow.h"
#include "ui_connectwindow.h"

ConnectWindow::ConnectWindow(QString *login, QString *passwd, QStatusBar *statusBar, QVector<Server*> *serverList, QWidget *parent) : QWidget(parent),
    ui(new Ui::ConnectWindow)
{
    ui->setupUi(this);
    this->login=login;
    this->passwd=passwd;
    this->statusBar=statusBar;
    this->serverList=serverList;
    comboBoxToServerListIndex = new QVector<int>();
    ui->connectSend->setPlaceholderText(tr("Envoyer une commande..."));
}

void ConnectWindow::on_connectButton_clicked()
{
    // Demande de connexion
    if (ui->serversComboBox->count() > 0)
    {
        ui->connectView->clear();
        ui->connectView->append(tr("connexion au serveur ")+serverList->at(comboBoxToServerListIndex->at(ui->serversComboBox->currentIndex()))->getIp()+tr(" demandée mais la fonctionnalité n'est pas encore implémentée"));
    }
    else
    {
        statusBar->showMessage(tr("Aucun serveur disponible, commencez par un scan"), 2500);
    }
}

void ConnectWindow::on_connectSend_returnPressed()
{
    // Envoit d'une commande au remote
    ui->connectSend->clear();
}

void ConnectWindow::updateServerComboList()
{
    ui->serversComboBox->clear();
    comboBoxToServerListIndex->clear();
    for (int i=0; i<serverList->size(); i++)
    {
        if (serverList->at(i)->getState() == Server::Reachable)
        {
            ui->serversComboBox->addItem(serverList->at(i)->getIp());
            comboBoxToServerListIndex->push_back(i);                    // To remember the index of the server in the serverList
        }
    }
}

ConnectWindow::~ConnectWindow()
{
    delete ui;
}
