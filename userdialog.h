#ifndef USERDIALOG_H
#define USERDIALOG_H

#include <QDialog>

namespace Ui {
class UserDialog;
}

class UserDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UserDialog(QString *login, QString *passwd, QWidget *parent = 0);
    ~UserDialog();

private slots:
    void on_submitButton_clicked();
    void on_loginTextEdit_returnPressed();
    void on_passwdTextEdit_returnPressed();

private:
    Ui::UserDialog *ui;
    QString *login;
    QString *passwd;
};

#endif // USERDIALOG_H
