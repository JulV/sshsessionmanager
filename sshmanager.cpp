#include "sshmanager.h"

SshManager::SshManager()
{

}

QString SshManager::execRemoteCmd(QString ip, QString login, QString password, QString cmd)
{
    ssh_session session;
    if (openSession(&session, ip.toStdString().c_str(), login.toStdString().c_str(), password.toStdString().c_str()) < 0)
    {
        return "erreur lors de la connexion";
    }
    QString result = requestCmdExec(session, cmd.toStdString().c_str());
    closeSession(&session);
    return result;
}

int SshManager::openSession(ssh_session *session, const char *ip, const char *login, const char *password)
{
    int verbosity = SSH_LOG_PROTOCOL;
    int port = 22;
    int rc;

    *session = ssh_new();                                // Creating a ssh session
    if (*session == NULL)                                // Is the session created
    {
        qDebug("Impossible de créet la session ssh");
        return -1;
    }
    ssh_options_set(*session, SSH_OPTIONS_HOST, ip);   // Set options for this session
    ssh_options_set(*session, SSH_OPTIONS_LOG_VERBOSITY, &verbosity);
    ssh_options_set(*session, SSH_OPTIONS_PORT, &port);
    rc=ssh_connect(*session);
    if (rc != SSH_OK)                                                       // Is the session ok
    {
        qDebug("Connection fail : %s", ssh_get_error(session));
        ssh_free(*session);
        return -1;
    }
    if (verifyKnownHost(*session) < 0)                                       // Verify the known hosts file
    {
        closeSession(session);
        return -1;
    }
    rc=ssh_userauth_password(*session, login, password);
    if (rc != SSH_AUTH_SUCCESS)                                             // Is the authentication is ok
    {
        qDebug("Erreur à l'authentification : %s", ssh_get_error(session));
        closeSession(session);
        return -1;
    }
    return 0;
}

void SshManager::closeSession(ssh_session *session)
{
    ssh_disconnect(*session);
    ssh_free(*session);
}

int SshManager::verifyKnownHost(ssh_session session)
{
    int state, rc;
    unsigned char *hash=NULL;
    char *hexa;
    size_t hlen;
    ssh_key serv_pubkey;

    state = ssh_is_server_known(session);
    rc = ssh_get_publickey(session, &serv_pubkey);
    if (rc < 0)
    {
        return -1;
    }
    rc = ssh_get_publickey_hash(serv_pubkey, SSH_PUBLICKEY_HASH_SHA1, &hash, &hlen);
    ssh_key_free(serv_pubkey);
    if (rc < 0)
    {
        return -1;
    }

    switch (state)
    {
    case SSH_SERVER_KNOWN_OK:
        break;  // Connection ok
    case SSH_SERVER_KNOWN_CHANGED:
        qDebug("a");
    case SSH_SERVER_FOUND_OTHER:
        qDebug("b");
    case SSH_SERVER_ERROR:
        free(hash);
        qDebug("c");
        return -1;
    case SSH_SERVER_FILE_NOT_FOUND:
    case SSH_SERVER_NOT_KNOWN:
        hexa = ssh_get_hexa(hash, hlen);
        qDebug("make a dialog to accept new server");
        // Ajouter le choix
        free(hexa);
        if (ssh_write_knownhost(session) < 0)
        {
            qDebug("Impossible d'ajouter le serveur à la liste des serveurs connus");
            free(hash);
            return -1;
        }
        break;
    }
    free(hash);
    return 0;
}

QString SshManager::requestCmdExec(ssh_session session, QString cmd)
{
    ssh_channel channel;
    int rc;
    char buffer[256]={'\0'};
    int nbytes;
    channel = ssh_channel_new(session);
    if (channel == NULL)
    {
        return "Cannot create a channel to host";
    }
    rc = ssh_channel_open_session(channel);
    if (rc != SSH_OK)
    {
        ssh_channel_free(channel);
        return "Cannot open the channel";
    }
    rc = ssh_channel_request_exec(channel, cmd.toStdString().c_str());
    if (rc != SSH_OK)
    {
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        return "Cannot send the command to host";
    }
    QString result;
    nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
    while (nbytes > 0)
    {
        result+=buffer;
        if (write(1, buffer, nbytes) != nbytes)
        {
            ssh_channel_close(channel);
            ssh_channel_free(channel);
            return "Cannot write data in buffer";
        }
        nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
    }
    if (nbytes < 0)
    {
        ssh_channel_close(channel);
        ssh_channel_free(channel);
        return "Negative amount of data!";
    }
    ssh_channel_send_eof(channel);
    ssh_channel_close(channel);
    ssh_channel_free(channel);
    return result;
}
