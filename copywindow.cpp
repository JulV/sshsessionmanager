#include "copywindow.h"
#include "ui_copywindow.h"

CopyWindow::CopyWindow(QString *login, QString *passwd, QStatusBar *statusBar, QVector<Server*> *serverList, QWidget *parent) : QWidget(parent),
    ui(new Ui::CopyWindow)
{
    ui->setupUi(this);
    this->login=login;
    this->passwd=passwd;
    this->statusBar=statusBar;
    this->serverList=serverList;
}

CopyWindow::~CopyWindow()
{
    delete ui;
}
